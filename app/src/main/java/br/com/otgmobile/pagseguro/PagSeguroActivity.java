package br.com.otgmobile.pagseguro;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_pag_seguro)
public class PagSeguroActivity extends AppCompatActivity {

    @Extra
    String url;

    @ViewById
    WebView webView;

    @AfterViews
    void afterViews(){
        webView.loadUrl(url);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            @SuppressWarnings("deprecation") // Na verdade esse é o método suportado por todas as versões do Android o novo método so na N
            public boolean shouldOverrideUrlLoading(WebView view, String url){
                Log.d("URL Redirect", url);
                checkUrl(url);
                return false; //Allow WebView to load url
            }
        });
    }

    void checkUrl(String url){
        if(url.contains("/attendences/")){
            setResultAndFinish();
        }
    }

    //Annotations já entende sem a anotação
    public void onBackPressed() {
        finish();
    }


    void setResultAndFinish(){
        setResult(RESULT_OK);
        finish();
    }
}
