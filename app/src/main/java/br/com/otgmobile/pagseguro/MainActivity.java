package br.com.otgmobile.pagseguro;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.UiThread;

import br.com.otgmobile.pagseguro.service.PaymentRequestService;


@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {

    public final static int PAGSEGURO_REQUEST_CODE = 333;


    @Bean
    PaymentRequestService paymentRequestService;

    @AfterViews
    void afterViews(){
        getPaymentRequest(1);
    }

    @Background
    void getPaymentRequest(Integer id){
        try {
            String url = paymentRequestService.request(id);
            if(url != null)
                showPaymentInterface(url);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @UiThread
    void showPaymentInterface(String url){
        Log.i("URL PAGSEGURO", url);
        Intent intent = PagSeguroActivity_.intent(this).url(url).get();
        startActivityForResult(intent,PAGSEGURO_REQUEST_CODE);
    }

    @OnActivityResult(PAGSEGURO_REQUEST_CODE)
    void onResult(int resultCode) {
        if(resultCode == Activity.RESULT_OK){

            Log.i("PAGSEGURO RETORNO", "RESULT_OK");
            /* O pagamento nesse ponto deve ter sido realizado
                Os dados do objeto devem ser carregados novamente
                para vir do servidor com o status PAGO

             */

        }
    }


}
