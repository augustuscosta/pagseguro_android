package br.com.otgmobile.pagseguro.service;

import android.content.Context;
import android.content.SharedPreferences;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import br.com.otgmobile.pagseguro.util.RestUtil;

/**
 * Created by augustuscosta on 22/06/16.
 */
@EBean
public class CookieService {

    @RootContext
    Context context;

    public void storedCookie(String cookie) {
        SharedPreferences sharedPref = context.getSharedPreferences(RestUtil.MOBILE_COOKIE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(RestUtil.MOBILE_COOKIE, cookie);
        editor.apply();
    }

    public String acquireCookie() {
        SharedPreferences sharedPref = context.getSharedPreferences(RestUtil.MOBILE_COOKIE, Context.MODE_PRIVATE);
        return sharedPref.getString(RestUtil.MOBILE_COOKIE, "");
    }

}
