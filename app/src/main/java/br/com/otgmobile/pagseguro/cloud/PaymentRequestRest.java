package br.com.otgmobile.pagseguro.cloud;

import org.androidannotations.rest.spring.annotations.Accept;
import org.androidannotations.rest.spring.annotations.Get;
import org.androidannotations.rest.spring.annotations.Path;
import org.androidannotations.rest.spring.annotations.RequiresCookie;
import org.androidannotations.rest.spring.annotations.Rest;
import org.androidannotations.rest.spring.annotations.SetsCookie;
import org.androidannotations.rest.spring.api.MediaType;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;

import br.com.otgmobile.pagseguro.model.PaymentRequest;
import br.com.otgmobile.pagseguro.util.RestUtil;

/**
 * Created by augustuscosta on 22/06/16.
 */

@Rest(rootUrl = RestUtil.ROOT_URL, converters = {MappingJacksonHttpMessageConverter.class})
@Accept(MediaType.APPLICATION_JSON)
public interface PaymentRequestRest {

    @RequiresCookie(RestUtil.MOBILE_COOKIE)
    @SetsCookie({RestUtil.MOBILE_COOKIE})
    @Get("produtos/{id}.json")
    PaymentRequest getPaymentRequest(@Path Integer id);

    String getCookie(String name);

    void setCookie(String name, String value);
}
